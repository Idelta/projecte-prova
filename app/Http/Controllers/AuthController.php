<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Auth;
use App\Http\Requests\LoginFormRequest;
use App\Http\Requests\CreateFormRequest;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class AuthController extends Controller
{

    public function index(){
        return view('registrar');
    }

    public function startSession(LoginFormRequest $request){

        if(Auth::attempt(['email' => $request->email, 'password' => $request->password], false)){
            return response()->json(['message' => 'correcte', 'token' => $request->token], 200);
        }else{
            return response()->json(['errors' => ['login' => ['Les dades no son correctes']]] , 422);
        }
        
    }

    public function create(CreateFormRequest $request)
    {
         User::create( [
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]); return response()->json('Registrat', 200);
    
       
    }


    public function destroy (){
        Auth::logout();
        return redirect('/login');
    }
}
