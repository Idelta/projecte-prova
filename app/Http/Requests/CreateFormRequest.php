<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required|string|min:6|max:20|confirmed',
            'password_confirmation' => 'required|string|min:6|max:20'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'El camp nom és obligatori',
            'email.required' => 'El camp correu és obligatori',
            'email.email' => 'El correu electrònic no correspon @',
            'password.required' => 'La contrasenya es obligatori',
            'password_confirmation.password' => 'La contrasenya no coincideix'
        ];
    }

}
