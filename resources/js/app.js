require('./bootstrap');

window.Vue = require('vue');

Vue.component('app', require('./components/AppComponent.vue').default);
Vue.component('menu-principal', require('./components/MenuComponent.vue').default);
Vue.component('logout', require('./components/LogoutComponent.vue').default);

import router from './routes'

const app = new Vue({
    el: '#app',
    router: router
});
