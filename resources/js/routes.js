import Vue from 'vue'
import VueRouter from 'vue-router'
// Importar components
import home from './../views/Home'
import login from './../views/Login'
import registrar from './../views/Registrar'
import dashboard from './../views/Dashboard'
import error from './../views/404'

Vue.use(VueRouter)

const router = new VueRouter({
    
    routes: [
        {
            path: '/',
            name: 'home',
            component: home
        },
        {
            path: '/login',
            name: 'login',
            meta: { 
                autentificat: false 
            },
            component: login
        },
        {
            path: '/registrar',
            name: 'registrar',
            component: registrar,
            props: true
        },
        {
            path: '/dashboard',
            name: 'dashboard',
            meta: { 
                autentificat: true 
            },
            component: dashboard,
            
        },
        {
            path: '*',
            component: error
        }

    ],
    mode: 'history'

});

router.beforeEach((to, from, next) => {
    if(to.meta.autentificat) {
        const token = document.querySelector('meta[name="csrf-token"]').content;
        if(to.meta.autentificat && localStorage.getItem('user_token') == token ) { 
            next();
        } else {
            next('login');
        }
    }else{ next(); }  
});

export default router;


  
