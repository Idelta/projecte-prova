<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;

Route::get('/{any}', [HomeController::class , 'index'])->where('any','.*');
Route::post('/login', [AuthController::class, 'startSession']);
Route::post('/session', [AuthController::class, 'session']);
Route::post('/registre', [AuthController::class, 'create']);
Route::post('/logout', [AuthController::class, 'destroy']);
